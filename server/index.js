const fs = require('fs');
const request = require('request');
const path = require('path');
const Vue = require('vue');
const VueTimeago = require('vue-timeago');
const renderer = require('vue-server-renderer').createRenderer();

Vue.use(VueTimeago, {
  name: 'timeago',
  locale: 'tr',
  locales: {
    'tr': require('date-fns/locale/tr')
  }
});

const YouTubeVideo = {
  props: {
    video: {
      type: Object,
      required: true,
    }
  },
  computed: {
    snippet() {
      return this.video.snippet;
    },
    coverPath() {
      return `https://i3.ytimg.com/vi/${this.video.id.videoId}/maxresdefault.jpg`;
    },
    videoUrl() {
      return `https://www.youtube.com/watch?v=${this.video.id.videoId}`;
    },
  },
  template: `
    <div class="video">
      <a :href="videoUrl" target="_blank">
        <img :src="coverPath" />
      </a>
      <h2>
        <a :href="videoUrl" target="_blank">{{snippet.title}}</a>
        <timeago :datetime="snippet.publishedAt" />
      </h2>
      <article>{{snippet.description}}</article>
    </div>
  `
};

const vueOptions = {
  data: {
    videos: [],
  },
  components: {
    'yt-video': YouTubeVideo,
  },
  template: `
    <section>
      <yt-video
        v-if="videos.length"
        v-for="video in videos"
        :video="video"
        key="video.id.videoId" />
    </section>
  `
};


const url = `https://www.googleapis.com/youtube/v3/search?key="$YT_KEY"&channelId="$YT_CHANNEL_ID"&part=snippet,id&order=date&maxResults=50&type=video`;
request(url, (err, res, body) => {
  if (err) throw err;

  vueOptions.data.videos = JSON.parse(body).items;
  const app = new Vue(vueOptions);

  renderer.renderToString(app, (err, html) => {
    if (err) throw err;
    const mydirname = path.join(path.dirname(process.mainModule.filename),'../themes/flexy/layout/_videos.html')
    fs.writeFileSync(mydirname, html);
  });
});
