---
title: SBC'ler(Raspberry Pi) için Yazılım Geliştirmeye Başlamak.
tags: |-

  - chrome
  - dev-tools
  - snippets
permalink: raspberry-pi-yazılım-geliştirme-ortamı-kurulumu
id: 2
updated: '2018-08-26 01:40:16'
date: 2018-08-26 01:40:16
---


**SBC'ler(Raspberry Pi) için Yazılım Geliştirme Sürecini Nasıl Kolaylaştırırız ?**
Bugün bu soruya cevap vermeye çalışacağım arkadaşlar eğer sadece bir laptop kullanıyorsanız fazladan bir monitor klavyeniz vs yoksa, raspberry pi ve türevi **"SBC"** ler için ilk kurulum aşamaları bile sıkıntı olabiliyor bir şekilde internete bağlanmanız ve cihaz ile iletişime geçmeniz lazım ssh, vnc gibi bunlar içinde bir şekilde cihazın ip adresini öğrenmeniz gerekiyor bunun için ip scanner kullanmanız gerekir yada cihaza bi şekilde static ip atamanız gerekir ki birden kendinizi yazılım geliştirmeye başlamadan  SBC'nizin ayarlarını yaparken bulursunuz gelin sizinle hem bu işlerin kolaylaştırılmasını hemde hali hazırda kullanmakta olduğunuz Visual Studio Code, Atom.io vs gibi akıllı kod editörlerden ayrılmadan yazılım  geliştirme sürecinize devam edin.

Bu yazıyı yazarken Ubuntu 18.04 LTS ve Raspberry Pi 3 B+  RASPBIAN STRETCH Kullandım.

İlk önce raspian stretch'i [indirmek için tıklayınız.](https://www.raspberrypi.org/downloads/raspbian/) İndirdikten sonra en az 8gb büyüklüğünde bir sd kart a işletim sisteminin imajını yazmanız gerekmekte ben bu işlem için [etcher.io](https://etcher.io/) kullandım. 

#### Debian ve Ubuntu Türevleri için.

1. Etcher debian reposunu ekliyoruz:

    ```bash
    echo "deb https://dl.bintray.com/resin-io/debian stable etcher" | sudo tee /etc/apt/sources.list.d/etcher.list
    ```

2. Güvenilir GPG anahtarınızı Ekliyoruz:

    ```bash
    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 379CE192D401AB61
    ```

3. Güncelleme ve yükleme için:

    ```bash
    sudo apt-get update
    sudo apt-get install etcher-electron
    ```
    
daha sonra **uygulama menüsünden** yada **alt+f2** yapıp etcher-electron diyerek programı çalıştırabilirsiniz indirdiğiniz sıkıştırılmış imajı çıkartıp programda dosya yolunu gösterin ve **sd kartınıza** yazın.

Bundan sonra işin güzel kısmı burda sd kartı raspberry pi a bağlamadan önce internet ayarlarını ve ssh bağlantınızı aktifleştiricez.
sd kartımız 2 kısıma ayrılmış olarak gözükmesi gerekiyor **rootfs** yazan kısıma girdikten sonra sağ tıklayıp bir terminal açalım ve aşağıdaki komutu çalıştıralım .

```bash sudo gedit /etc/wpa_supplicant/wpa_supplicant.conf```

Aşağıdaki kodu oraya kopyalayın ve kendi kablosuz ağ adınızı ve şifrenizi yazın ve kaydedip çıkın

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=GB

network={
    ssid="Kablosuz-Ag-Adı"
    psk="Şifreniz"
    key_mgmt=WPA-PSK
}
```

SSH bağlantınızı aktifleştirmek için **boot** bölümüne girin ve 
```bash
sudo gedit ssh
```

komutunu çalıştırın ve boş bir şekilde kaydedip çıkın ve kartı raspberry pi' ınıza takın ve boot olmasını bekleyin. cihazın açıldığını anlamak için 

```bash
ping raspberrypi.local
```
yazmanız yeterli.

 Gördüğünüz gibi arkadaşlar cihazın IP'sini bulmamıza gerek yok öncelikle bundan bahsedeyim mDNS yani multicast DNS hostname'den IP adresini çözerek o cihaza ulaşmanızı sağlar yani cihazın ip adresini öğrenmeye yada static ip yapmak suretiyle bir işkenceye girmenize gerek yok.

 Eğer ping almışsak yolumuza şu şekilde devam ediyoruz. Bilgisayarınızda Visual Studio Code yüklü olduğunu varsayıyorum. 

Uzaktan raspberry pi dosya sistemine erişmek için belli protokoller var ben sftp yi tercih ediyorum **Ubuntu dosya yöneticisi Nautilus**'a da bu protokoller entegre edilmiş.

**Nautilus**'u açtığınızda diğer kısımlar var oraya tıkladığınızda alt kısımda uzak sunucuya bağlan yazan bir kutucuk var biz buraya 

```bash
sftp://raspberrypi.local
```
yazıp kullanıcı adı ve şifre yazıp giriyoruz .
**(Default kullanıcı adı pi ve şifre raspberry)**

/home/pi/ kısayolunu izlediğimizde ana dizine ulaşmış oluyoruz. 

Burda bir deneme adında bir klasör oluşturun daha sonra Visual Studio Code ile o klasörü açıp istediğiniz dilde bir deneme kodu ekleyip Visual Studio Code entegre terminalini açıp 

```bash
ssh pi@raspberrypi.local
```

yazıp bağlanın artık ana dizindesiniz programınızın açık olduğu bir akıllı editör ve bir terminale sahipsiniz gerisi sizde, **İyi Çalışmalar.**

Muhammet Şükrü Demir.
