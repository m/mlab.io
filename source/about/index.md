---
title: About
tags: |-

  - chrome
  - dev-tools
  - snippets
page.permalink: /about
comments: false
updated: '2021-07-08 16:30:00'
date: 2021-07-08 16:30:00
---

**Muhammet Şükrü Demir**


|**Kişisel Bilgiler**|                              |
|-------------------|-------------------------------|
| **Doğum Tarihi:** | 15/03/1996                    |
| **E-Posta:**      | demir.mhmmt[at]gmail.com        |
| **Gitlab:**       | gitlab.com/m                  |
| **LinkedIn**      | www.linkedin.com/in/mhmmtdmr/ |

|**Eğitim Bilgileri**|                                       |                                                  |                                     |              |
|----------------|-------------------------------------------|--------------------------------------------------|-------------------------------------|--------------|
| **Üniversite** | Niğde Ömer Halisdemir Üniversitesi, Niğde | Elektrik-Elektronik Mühendisliği (%30 İngilizce) | Lisans,  GPA 3.65                   | 2018-2021    |
|                | Niğde Ömer Halisdemir Üniversitesi, Niğde | Mekatronik Mühendisliği                          | Lisans,  GPA 3.72 (Bölüm Birincisi) | 2015-2020    |
| **Lise**       | Alaca Anadolu Öğretmen Lisesi, Çorum      |                                                  |                                     | 2010-2014    |

|**Yabancı Dil**|               | |                            |
|-----------|----------------|----------------|--------------|
| İngilizce | Okuma: Çok iyi | Yazma: Çok iyi | Konuşma: iyi |


|**Bilgisayar Bilgileri**|
|---------------------------------------------------------------------------------------------------------------|
| İşletim Sistemleri                                                                                            |
| Windows                                                                                                       |
| Linux(Ubuntu, Debian, Raspbian)                                                                               |
| Programlama Dilleri                                                                                           |
| C, C++, Javascript, Node.JS, Golang, MATLAB, MATLAB/Simulink, ROS (Robot Operating System), QT Framework, Qml |
| Programlar                                                                                                    |
| MATLAB, Altium Designer, Eagle, Multisim, Proteus, MikroC, XC8, XC16, V-REP, Gazebo, Qt, AutoCAD, Solidworks  |

|**Projeler**    |
|-------------------------------------------------------------------------|
| Otonom Sürüş Yazılımı Geliştirme Ar-Ge Projesi ( KOSGEB Destekli devam ediyor)|
| Akıllı Kilit ( Node.JS ve Raspberry pi)|
| Güzergâh Oluşturucu ve Görselleştirici Web Sitesi Tasarımı ( robotics.ohu.edu.tr/ppv ) Google Maps API ve Bootstrap teknolojileri ile tasarlanmıştır.  Harita üzerinde seçilen noktalardan bir rota oluşturur ve koordinatlarını sırası ile metin kutusu içerisinde verebilir. Gerçek zamanlı yada simülasyon sonucu elde edilen uçuş koordinatlarını görselleştirebilir. |
| 12V 5A ve 5V 2A Çıkışlı DC-DC Çevirici Kartı. (24V Nominal Giriş Gerilimi) |
| 300W Fırçalı Motor Sürücü Tasarımı ve Pozisyon Kontrolü (**Bitirme Tezi**) Atmega328P Mikrodenetleyicisi ve VNH7070 Motor sürücü entegresi kullanılmıştır. Raspberry PI ile kullanılmak için tasarlanmıştır.      |
| Aruco Marker Kullanılarak kalibre edilmiş kamera ve derinlik sensörü karşılaştırılması (Kinect) |

|**Hobiler**|
|----------------|
| Fotoğrafçılık  |
| Masa Tenisi    |
